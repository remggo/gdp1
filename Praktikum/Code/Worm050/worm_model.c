// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//

#include <curses.h>
#include "worm.h"
#include "worm_model.h"

// Initialize the worm
extern enum ResCodes initializeWorm(struct worm* aworm, int len_max, struct pos headpos, enum WormHeading dir, enum ColorPairs color) {

  enum ResCodes resCodes;   

  //Local variables for loops etc. 
  int i; 
  // Initialize last usable index to len_max -1: theworm_maxindex
  aworm->maxindex  = len_max - 1; 

  // Initialize headindex: theworm_headindexi
  // aworm->headindex <=> (*aworm).headindex
  aworm->headindex = 0; 

  // Mark all elements as unused in the arrays of positions
  // theworm_wormpos_y[] and theworm_wormpos_x[]
  // An unused position in the array is marked with code UNUSED_POS_ELEM
  for(i = 1; i <= aworm->maxindex; i++)
  {
    aworm->wormpos[i].y = UNUSED_POS_ELEM;
    aworm->wormpos[i].x = UNUSED_POS_ELEM; 
  }

  // Initialize position of worms head, make use of struct pos
  aworm->wormpos[aworm->headindex] = headpos;

  // Initialize the heading of the worm
  setWormHeading(aworm, dir);

  // Initialze color of the worm
  aworm->wcolor = color;
  resCodes = RES_OK;
  return resCodes;
}

// Show the worms's elements on the display
// Simple version
extern void showWorm(struct worm* aworm) {
  // Due to our encoding we just need to show the head element
  // All other elements are already displayed
  placeItem(
      aworm->wormpos[aworm->headindex],
      SYMBOL_WORM_INNER_ELEMENT,
      aworm->wcolor);
}

extern void cleanWormTail(struct worm* aworm)
{
  int tailindex; 
  // theworm_maxindex + 1 because of zeroindex
  tailindex = (aworm->headindex + 1) % (aworm->maxindex + 1);

  // Is array element at tailindex already in use?
  if(aworm->wormpos[tailindex].x != UNUSED_POS_ELEM)
  {
    //YES: Go get that Symbol wiped!
    placeItem(
        aworm->wormpos[tailindex],
        SYMBOL_FREE_CELL,
        COLP_FREE_CELL);
  }
}

extern void moveWorm(struct worm* aworm, enum GameStates* agame_state) {

  struct pos headpos;
  // Compute and store new head position according to current heading.
  headpos.x = aworm->wormpos[aworm->headindex].x + aworm->dx;
  headpos.y = aworm->wormpos[aworm->headindex].y + aworm->dy;

  // Check if we would leave the display if we move the worm's head according
  // to worm's last direction.
  // We are not allowed to leave the display's window.
  if (headpos.x < 0) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.x > getLastCol() ) { 
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.y < 0) {  
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.y > getLastRow() ) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else {
    // We will stay within bounds.
    // Check if the worm's head will collide with itself at the new Position
    if ( isInUseByWorm(aworm, headpos))
    {
      *agame_state = WORM_CROSSING; 
    }
    if ( *agame_state == WORM_GAME_ONGOING )
    {
      // Nothings hit, we're good to go and save those coordinates!
      if (aworm->headindex == aworm->maxindex)
      {
        aworm->headindex = 0; 
      }
      else
      {
        aworm->headindex++;
      }
      // Save them!
      aworm->wormpos[aworm->headindex].x = headpos.x;
      aworm->wormpos[aworm->headindex].y = headpos.y;

    }
  }
}

// For Collision Detection
extern bool isInUseByWorm(struct worm* aworm, struct pos new_headpos)
{
  int i;
  bool collision = false;
  i = 0;
  do
  {
    if(aworm->wormpos[i].x == new_headpos.x && aworm->wormpos[i].y == new_headpos.y)
    {
      collision = true;
      break;
    }
    i++;
  }
  while(i < aworm->maxindex  && aworm->wormpos[i].x != UNUSED_POS_ELEM);
  return collision;
}

// Setters
extern void setWormHeading(struct worm* aworm, enum WormHeading dir) {
  switch(dir) {
    case WORM_UP:   // User wants up
      aworm->dx = 0;
      aworm->dy = -1;
      break;
    case WORM_DOWN:  // User wants down
      aworm->dx = 0;
      aworm->dy = 1;
      break;
    case WORM_RIGHT:  // User wants left
      aworm->dx = 1;
      aworm->dy = 0; 
      break;
    case WORM_LEFT:   // User wants right
      aworm->dx = -1; 
      aworm->dy = 0; 
      break;
    default:
      break;
  }
} 

struct pos getWormHeadPos(struct worm* aworm) 
{
  //Structures are passed by Value!
  // -> we return a copy here
  return aworm->wormpos[aworm->headindex];
}
