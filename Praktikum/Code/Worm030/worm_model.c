// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//

#include <curses.h>
#include "worm.h"
#include "board_model.h"
#include "worm_model.h"

// The worm model
// Data defining the worm
int theworm_wormpos_y[WORM_LENGTH];  // Array of y positions for worm elements
int theworm_wormpos_x[WORM_LENGTH];  // Array of x positions for worm elements

int theworm_maxindex; //Last usable index for the arrays
//theworm_wormpos_x & .._y
int theworm_headindex; //Index of Worms head position in the arrays
// 0 <= theworm_headindex <= theworm_maxindex

// The current heading of the worm
// These are offsets from the set {-1,0,+1}
int theworm_dx;
int theworm_dy;

enum ColorPairs theworm_wcolor; 

// Initialize the worm
extern enum ResCodes initializeWorm(int len_max, int headpos_x, int headpos_y, enum WormHeading dir, enum ColorPairs color) {

  enum ResCodes resCodes;   

  //Local variables for loops etc. 
  int i; 
  // Initialize last usable index to len_max -1: theworm_maxindex
  theworm_maxindex = len_max - 1; 

  // Initialize headindex: theworm_headindex
  theworm_headindex = 0; 

  // Mark all elements as unused in the arrays of positions
  // theworm_wormpos_y[] and theworm_wormpos_x[]
  // An unused position in the array is marked with code UNUSED_POS_ELEM
  for(i = 1; i < WORM_LENGTH; i++)
  {
    theworm_wormpos_y[i] = UNUSED_POS_ELEM;
    theworm_wormpos_x[i] = UNUSED_POS_ELEM; 
  }

  // Initialize position of worms head
  theworm_wormpos_x[theworm_headindex] = headpos_x;
  theworm_wormpos_y[theworm_headindex] = headpos_y; 

  // Initialize the heading of the worm
  setWormHeading(dir);

  // Initialze color of the worm
  theworm_wcolor = color;
  resCodes = RES_OK;
  return resCodes;
}

// Show the worms's elements on the display
// Simple version
extern void showWorm() {
  // Due to our encoding we just need to show the head element
  // All other elements are already displayed
  placeItem(
      theworm_wormpos_x[theworm_headindex],
      theworm_wormpos_y[theworm_headindex],
      SYMBOL_WORM_INNER_ELEMENT,
      theworm_wcolor);
}

extern void cleanWormTail()
{
  int tailindex; 
  // theworm_maxindex + 1 because of zeroindex
  tailindex = (theworm_headindex + 1) % (theworm_maxindex + 1);

  // Is array element at tailindex already in use?
  if(theworm_wormpos_x[tailindex] != UNUSED_POS_ELEM)
  {
    //YES: Go get that Symbol wiped!
    placeItem(
        theworm_wormpos_x[tailindex], 
        theworm_wormpos_y[tailindex],
        SYMBOL_FREE_CELL,
        COLP_FREE_CELL);

  }
}

extern void moveWorm(enum GameStates* agame_state) {

  int headpos_x, headpos_y;
  // Compute and store new head position according to current heading.
  headpos_x = theworm_wormpos_x[theworm_headindex] + theworm_dx;
  headpos_y = theworm_wormpos_y[theworm_headindex] + theworm_dy;

  // Check if we would leave the display if we move the worm's head according
  // to worm's last direction.
  // We are not allowed to leave the display's window.
  if (headpos_x < 0) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos_x > getLastCol() ) { 
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos_y < 0) {  
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos_y > getLastRow() ) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else {
    // We will stay within bounds.
    // Check if the worm's head will collide with itself at the new Position
    if ( isInUseByWorm(headpos_x, headpos_y))
    {
      *agame_state = WORM_CROSSING; 
    }
    if ( *agame_state == WORM_GAME_ONGOING )
    {
      // Nothings hit, we're good to go and save those coordinates!
      if (theworm_headindex == theworm_maxindex)
      {
        theworm_headindex = 0; 
      }
      else
      {
        theworm_headindex++;
      }
      // Save them!
      theworm_wormpos_x[theworm_headindex] = headpos_x;
      theworm_wormpos_y[theworm_headindex] = headpos_y;

    }
  }
}

// For Collision Detection
extern bool isInUseByWorm(int new_headpos_x, int new_headpos_y)
{
  int i;
  bool collision = false;
  i = theworm_headindex; 
/*  Better: 
   i = 0;
   do
   {
     if(theworm_wormpos_x[i] == new_headpos_x && theworm_wormpos_y[i] == new_headpos_y);
     {
        collision = true;
        break;
      }
      i++;
   }
   while(i < WORM_LEFT && theworm_wormpos_x[i] != UNUSED_POS_ELEM)
   return collision;
 */
  
  do
  {
    if( theworm_wormpos_x[i] == new_headpos_x && theworm_wormpos_y[i] == new_headpos_y)
    {
      collision = true; 
    }
    i++; 
  }
  while ( i != (theworm_maxindex + 1) &&
      theworm_wormpos_x[i] != UNUSED_POS_ELEM);
  return collision;
}

// Setters
extern void setWormHeading(enum WormHeading dir) {
  switch(dir) {
    case WORM_UP:   // User wants up
      theworm_dx = 0;
      theworm_dy = -1;
      break;
    case WORM_DOWN:  // User wants down
      theworm_dx = 0;
      theworm_dy = 1;
      break;
    case WORM_RIGHT:  // User wants left
      theworm_dx = 1;
      theworm_dy = 0; 
      break;
    case WORM_LEFT:   // User wants right
      theworm_dx = -1; 
      theworm_dy = 0; 
      break;
    default:
      break;
  }
} 


