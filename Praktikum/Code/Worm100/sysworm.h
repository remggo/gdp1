// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//
// Functions for manipulating automated system worms

#ifndef _SYSWORM_H
#define _SYSWORM_H

// Dimensions and bounds
#define WORM_INITIAL_SYSWORM_LENGTH 8  // Initial length for sysworms



struct f{
    int f;
    enum Direction direction;
};

extern enum ResCodes
initializeSyswormsArray(struct game_options* somegops, struct worm** ptrToSyswormsArray);
extern void cleanupSyswormsArray(struct worm* ptrToSyswormsArray);
extern enum ResCodes
initializeAndShowAutoWorm(struct board* aboard, struct game_options* somegops, struct worm* aworm, int len, enum ColorPairs color);
extern enum ResCodes
enableSysworms(struct game_options* somegops, struct board* aboard, struct worm sysworms[]);
extern void disableSysworms(struct game_options* somegops, struct board* aboard, struct worm sysworms[]);
extern bool isGoodHeading(struct board* aboard, struct pos next_pos);
extern enum ResCodes computeDirection(struct board* aboard, struct game_options* somegops, struct worm* aworm);
extern bool getPossibleDirections(struct worm *aworm, struct board* aboard, struct pos cur_pos, struct pos dest_pos);
extern struct pos chooseNewPos(struct worm* aworm, struct board* aboard, struct pos cur_pos);
extern struct pos addDirection(struct pos base_pos, enum Direction dir, bool backwards);
extern void fillNode(struct pos current, struct pos food_pos, struct board* aboard, int prev_Cost, enum Direction dir);
extern int getHeuristic(struct pos pos_start, struct pos pos_finish);
extern struct pos addDirectionXY(struct pos base_pos, int dx, int dy);
void reverseSearchMoveDeltas(struct worm* aworm, struct board* aboard, struct pos start, struct pos finish);
#endif  // ifndef _SYSWORM_H
