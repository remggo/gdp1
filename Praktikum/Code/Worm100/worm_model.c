// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//

#include <curses.h>
#include <stdlib.h>
#include "messages.h"
#include "worm.h"
#include "worm_model.h"

// Initialize the worm
extern enum ResCodes initializeWorm(struct worm* aworm, int len_max, int len_cur, struct pos headpos, enum WormHeading dir, enum ColorPairs color) {

  enum ResCodes resCodes;   

  //Local variables for loops etc. 
  int i; 
  // Current last usable index in array. May grow up to maxindex.
  aworm->maxindex = len_max - 1; 
  aworm->cur_lastindex  = len_cur - 1; 

  // Initialize headindex: theworm_headindexi
  // aworm->headindex <=> (*aworm).headindex
  aworm->headindex = 0; 
  
  //Initialize array for element positions
  if( (aworm->wormpos = malloc(sizeof(struct pos) * len_max)) == NULL)
  {
    showDialog("Abbruch: Zu wenig Speicher", "Bitte eine Taste druecken");
    exit(RES_FAILED);
  }

  // Mark all elements as unused in the arrays of positions
  // theworm_wormpos_y[] and theworm_wormpos_x[]
  // An unused position in the array is marked with code UNUSED_POS_ELEM
  for(i = 1; i <= aworm->maxindex; i++)
  {
    aworm->wormpos[i].y = UNUSED_POS_ELEM;
    aworm->wormpos[i].x = UNUSED_POS_ELEM; 
  }

  // Initialize position of worms head, make use of struct pos
  aworm->wormpos[aworm->headindex] = headpos;

  // Initialize the heading of the worm
  setWormHeading(aworm, dir);

  // Initialze color of the worm
  aworm->wcolor = color;
  resCodes = RES_OK;
  return resCodes;
}

void cleanupWorm(struct worm* aworm) {
  free(aworm->wormpos);
}

void removeWorm(struct board* aboard, struct worm* aworm){
  int i;
  
  for(i = 0; i <= aworm->cur_lastindex; i++){
    if(aworm->wormpos[i].y != UNUSED_POS_ELEM)
    {
      placeItem(aboard, aworm->wormpos[i], BC_FREE_CELL, SYMBOL_FREE_CELL, COLP_FREE_CELL);
    }
  }
}

// Show the worms's elements on the display
// Simple version
extern void showWorm(struct board* aboard, struct worm* aworm) {
  // Due to our encoding we just need to show the head element
  // All other elements are already displayed
  int i;
  int tailindex;
  int wormLen = getWormLength(aworm); 

  // theworm_maxindex + 1 because of zeroindex
  tailindex = (aworm->headindex + 1) % (aworm->cur_lastindex + 1);
  //wormLen for Performance *yeeei*:D
  for ( i = 0; i < wormLen; i++)
  {
    if(aworm->wormpos[i].y != UNUSED_POS_ELEM)
    {
      char c;
      if(i == aworm->headindex)
      {
        c = SYMBOL_WORM_HEAD_ELEMENT;
      }
      else if(i == tailindex)
      {
        c = SYMBOL_WORM_TAIL_ELEMENT; 
      }
      else
      {
        c = SYMBOL_WORM_INNER_ELEMENT;
      }
      placeItem(
          aboard,
          aworm->wormpos[i],
          BC_USED_BY_WORM,
          c,
          aworm->wcolor);
    }
  }
}
extern int getWormLength(struct worm* aworm)
{
  return aworm->cur_lastindex + 1; 
}

extern void growWorm(struct worm* aworm, enum Boni growth)
{
  if(aworm->cur_lastindex + growth <= aworm->maxindex)
  {
    aworm->cur_lastindex += growth;
  }
  else
  {
    aworm->cur_lastindex = aworm->maxindex;
  }
}

extern void cleanWormTail(struct board* aboard, struct worm* aworm)
{
  int tailindex; 
  // theworm_maxindex + 1 because of zeroindex
  tailindex = (aworm->headindex + 1) % (aworm->cur_lastindex + 1);

  // Is array element at tailindex already in use?
  if(aworm->wormpos[tailindex].x != UNUSED_POS_ELEM)
  {
    //YES: Go get that Symbol wiped!
    placeItem(
        aboard,
        aworm->wormpos[tailindex],
        BC_FREE_CELL,
        SYMBOL_FREE_CELL,
        COLP_FREE_CELL);
  }
}

void removeFoodItemFromList(struct board* aboard, struct pos pos){
  int i=0;
  while(aboard->food_item_list[i].x != LAST_POS_ELEM){
    if(aboard->food_item_list[i].x == pos.x && aboard->food_item_list[i].y == pos.y){
      aboard->food_item_list[i].x = UNUSED_POS_ELEM;
      aboard->food_item_list[i].y = UNUSED_POS_ELEM;
    }
    i++;
  }

}
extern void moveWorm(struct board* aboard, struct worm* aworm, enum GameStates* agame_state) {

  struct pos headpos;
  // Compute and store new head position according to current heading.
  headpos.x = aworm->wormpos[aworm->headindex].x + aworm->dx;
  headpos.y = aworm->wormpos[aworm->headindex].y + aworm->dy;

  // Check if we would leave the display if we move the worm's head according
  // to worm's last direction.
  // We are not allowed to leave the display's window.
  if (headpos.x < 0) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.x > getLastColOnBoard(aboard) ) { 
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.y < 0) {  
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else if (headpos.y > getLastRowOnBoard(aboard) ) {
    *agame_state = WORM_OUT_OF_BOUNDS;
  } else {
    // We will stay within bounds.
    // Check if the worm's head will collide with itself at the new Position
    switch( getContentAt(aboard, headpos) )
    {
      case BC_FOOD_1:
        *agame_state = WORM_GAME_ONGOING;
        growWorm(aworm, BONUS_1);
        decrementNumberOfFoodItems(aboard);
        removeFoodItemFromList(aboard, headpos);
        break;
      case BC_FOOD_2:
        *agame_state = WORM_GAME_ONGOING;
        growWorm(aworm, BONUS_2);
        decrementNumberOfFoodItems(aboard);
        removeFoodItemFromList(aboard, headpos);
        break;
      case BC_FOOD_3:
        *agame_state = WORM_GAME_ONGOING;
        growWorm(aworm, BONUS_3);
        decrementNumberOfFoodItems(aboard); 
        removeFoodItemFromList(aboard, headpos);
        break;
      case BC_BARRIER:
        *agame_state = WORM_CRASH;
        break;
      case BC_USED_BY_WORM:
        *agame_state = WORM_CROSSING;
        break;
      default: //Default Needed to avoid "You didn't used all your BC_ Elements" Warning
        ; //Do Nothing!
    }
    if ( *agame_state == WORM_GAME_ONGOING)
    {
      aworm->headindex++;
      if(aworm->headindex > aworm->cur_lastindex)
      {
        aworm->headindex = 0; 
      }
      aworm->wormpos[aworm->headindex] = headpos; 
    }
  }
}

// Setters
extern void setWormHeading(struct worm* aworm, enum WormHeading dir) {
  switch(dir) {
    case WORM_UP:   // User wants up
      aworm->dx = 0;
      aworm->dy = -1;
      break;
    case WORM_DOWN:  // User wants down
      aworm->dx = 0;
      aworm->dy = 1;
      break;
    case WORM_RIGHT:  // User wants left
      aworm->dx = 1;
      aworm->dy = 0; 
      break;
    case WORM_LEFT:   // User wants right
      aworm->dx = -1; 
      aworm->dy = 0; 
      break;
    default:
      break;
  }
} 

struct pos getWormHeadPos(struct worm* aworm) 
{
  //Structures are passed by Value!
  // -> we return a copy here
  return aworm->wormpos[aworm->headindex];
}
