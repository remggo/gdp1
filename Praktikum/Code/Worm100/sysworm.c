// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//
// Functions for manipulating automated system worms

#include <curses.h>
#include <stdbool.h>
#include <stdlib.h>

#include "worm.h"
#include "worm_model.h"
#include "board_model.h"
#include "options.h"
#include "messages.h"

#include "sysworm.h"

enum ResCodes initializeSyswormsArray(struct game_options* somegops, struct worm** ptrToSyswormsArray)
{
  enum ResCodes res_code = RES_OK;

  *ptrToSyswormsArray = malloc(somegops->sysworms_nr * sizeof(struct worm));
  if(*ptrToSyswormsArray == NULL)
  {
    res_code = RES_NO_MEMORY;
  }
  return res_code;

}

void cleanupSyswormsArray(struct worm* ptrToSyswormsArray)
{
  free(ptrToSyswormsArray);
}

enum ResCodes initializeAndShowAutoWorm(struct board* aboard, struct game_options* somegops, struct worm* aworm, int len, enum ColorPairs color)
{
  enum ResCodes res_code = RES_OK;
  //set wormpos[0] to new headPos
  struct pos new_headpos;
  res_code = findFreeCell(aboard, &new_headpos);
  //Direction set as WORM_UP... gets overwritten in computeDirection
  if(res_code == RES_OK)
  {
    res_code = initializeWorm(aworm, (aboard->last_row + 1)*(aboard->last_col + 1), len, new_headpos, WORM_UP, color);
    showWorm(aboard, aworm);
    computeDirection(aboard, somegops, aworm);
  }
  return res_code;
}

enum ResCodes enableSysworms(struct game_options* somegops, struct board* aboard, struct worm sysworms[])
{
  enum ResCodes res_code = RES_OK;

  int w;
  enum ColorPairs color;
  for( w = 0; w < somegops->sysworms_nr; w++)
  {
    color = FIRST_AUTO_COLP + (rand() % (LAST_AUTO_COLP - FIRST_AUTO_COLP +1 ));
    initializeAndShowAutoWorm(aboard, somegops, &sysworms[w], WORM_INITIAL_LENGTH, color);
  }

  return res_code;
}

void disableSysworms(struct game_options* somegops, struct board* aboard, struct worm sysworms[])
{
  int w;
  for(w = 0; w < somegops->sysworms_nr; w++)
  {
    removeWorm(aboard, &sysworms[w]);
    cleanupWorm(&sysworms[w]);
  }
}

int getModulus(int number)
{
  if(number > 0)
  {
    return number;
  }
  else
  {
    return -1 * number;
  }
}

bool isGoodHeading(struct board* aboard, struct pos next_pos)
{
  // @nnn; replace the following line by some useful code
  //
  bool result;
  if(
      next_pos.x < 0 ||
      next_pos.y < 0 ||
      next_pos.x > getLastColOnBoard(aboard) ||
      next_pos.y > getLastRowOnBoard(aboard) )
  {
    result = false;
  }
  else
  {
    switch(aboard->cells[next_pos.y][next_pos.x])
    {
      case BC_FREE_CELL:
      case BC_FOOD_1:
      case BC_FOOD_2:
      case BC_FOOD_3:
        result = true;
        break;
      case BC_USED_BY_WORM:
      case BC_BARRIER:
        result = false;
        break;
      default:
        result = false;
        break;
    }
  }
  return result;
}

int getDistance(struct pos pos)
{
  int result;
  int x = getModulus(pos.x);
  int y = getModulus(pos.y);
  if(x == 0)
  {
    result = y;
  }
  else if( y == 0)
  {
    result = x;
  }
  else
  {
    result = x + y;
  }
  return result;

}

struct pos inversePos(struct pos pos)
{
  pos.x = pos.x * -1;
  pos.y = pos.y * -1;
  return pos;
}

enum ResCodes setGoodDirs(struct board* aboard, struct worm* aworm)
{
  enum ResCodes res_code = RES_OK;
  struct pos possibleDirs[] = {{-1, 0}, {1, 0}, {0, 1}, {0, -1}};
  struct pos goodDirs[] = {{0,0},{0,0},{0,0},{0,0}};
  struct pos new_pos;
  bool foundDir = false, currentHeadingGood = false;
  int i, j = 0;
  int cur_headindex = aworm->headindex;
  for(i = 0; i < 4; i++)
  {
    new_pos = aworm->wormpos[cur_headindex];
    new_pos.x += possibleDirs[i].x;
    new_pos.y += possibleDirs[i].y;
    if(isGoodHeading(aboard, new_pos))
    {
      goodDirs[j] = possibleDirs[i];
      j++;
    }
  }
  //Wähle eine gute Richtung aus!
  if(j != 0)
  {
    foundDir = true;
    i = rand() % j;
    aworm->dx = goodDirs[i].x;
    aworm->dy = goodDirs[i].y;
  }
  else
  {
    foundDir = currentHeadingGood;
  }
  if(!foundDir)
  {
    res_code = RES_NO_FREE_DIRECTION_ERROR;
  }
  return res_code;
}


enum ResCodes computeDirection(struct board* aboard, struct game_options* somegops, struct worm* aworm)
{

  /**Declaration**/
  enum ResCodes res_code;
  int cur_headindex;
  //struct new_dir;
  int test;
  bool currentHeadingGood, foundDir;
  struct pos new_pos;
  struct pos cur_pos;
  struct pos start_pos;
  struct pos nearest_food_delta = {100, 100};
  struct pos temppos;
  bool hasDirection;
  int temp_dx = 0, temp_dy = 0;
  struct pos nearest_food_pos;
  int i = 0;


  /**Definitions**/
  res_code = RES_OK;
  cur_headindex = aworm->headindex;
  cur_pos = aworm->wormpos[cur_headindex];
  start_pos.x = cur_pos.x;
  start_pos.y = cur_pos.y;
  new_pos = addDirectionXY(cur_pos, aworm->dx, aworm->dy);
  currentHeadingGood = isGoodHeading(aboard, new_pos);
  foundDir = currentHeadingGood;

  test = rand() % 100;
  if(somegops->difficulty != 0)
  {
      while( aboard->food_item_list[i].x != LAST_POS_ELEM)
    {
      if(aboard->food_item_list[i].x != UNUSED_POS_ELEM)
      {
        temppos.x = cur_pos.x - aboard->food_item_list[i].x;
        temppos.y = cur_pos.y - aboard->food_item_list[i].y;
        if( getDistance(temppos) < getDistance(nearest_food_delta) )
        {
          nearest_food_delta = temppos;
          nearest_food_pos = aboard->food_item_list[i];
        }
      }
      i++;
    }
    //nearest_food_delta hat dX und dY gespeichtert
  }

  switch (somegops->difficulty)
  {
    case 0:
      if( test <= 20 || !currentHeadingGood)
      {
        res_code = setGoodDirs(aboard, aworm);
      }
      break;
    case 1:

      // foundDir = false;
      if(nearest_food_delta.x == 100 && nearest_food_delta.y == 100)
      {
        //No food items found?
        somegops->difficulty = 0; //Set difficulty @ random: no more Food items left!
      }
      nearest_food_delta = inversePos(nearest_food_delta);
      if(nearest_food_delta.x != 0)
      {
        temp_dx = nearest_food_delta.x / getModulus(nearest_food_delta.x);
      }
      else {
        temp_dx = 0;
      }
      if(nearest_food_delta.y != 0)
      {
        temp_dy = nearest_food_delta.y / getModulus(nearest_food_delta.y);
      }
      else {
        temp_dy = 0;
      }
      //Testweise Diagonal laufen lassen...
      temppos.x = cur_pos.x + temp_dx;
      // temppos.y = cur_pos.y + temp_dy;
      temppos.y = cur_pos.y;
      if(isGoodHeading(aboard, temppos))
      {
        aworm->dx = temp_dx;
        aworm->dy = 0;
        foundDir = true;
      }
      else {
        temppos.x = cur_pos.x;
        temppos.y = cur_pos.y + temp_dy;
        if( isGoodHeading(aboard, temppos))
        {
          aworm->dx = 0;
          aworm->dy = temp_dy;
          foundDir = true;
        }

      }
      break;

    case 2:
      //A* Algorithm
      /*
         Open List: nodes.isActive == true && nodes.isClosed == false
         Closed List: nodes.isActive == true && nodes.isClosed == true
         */
      fillNode(cur_pos, nearest_food_pos, aboard, 0, START);
      new_pos.x = cur_pos.x;
      new_pos.y = cur_pos.y; //Start at current Position
      while(new_pos.x != nearest_food_pos.x && new_pos.y != nearest_food_pos.y){
          aboard->nodes[cur_pos.y][cur_pos.x].isActive = true; //Set Current Node in OpenList
          hasDirection = getPossibleDirections(aworm, aboard, cur_pos, nearest_food_pos);
          if(!hasDirection){
            //Worm is stuck, do nothing
            //Do not change direction. Let them worms fail!
            break;
          }
          //possible directions are found. Choose wisely
          new_pos = chooseNewPos(aworm, aboard, cur_pos);
          aboard->nodes[new_pos.y][new_pos.x].isClosed = true;
        }
      reverseSearchMoveDeltas(aworm, aboard, new_pos, start_pos);
      initializeNodes(aboard);
      break;

    default:
      somegops->difficulty = 0;
  }

  if(!foundDir)
  {
    res_code = setGoodDirs(aboard, aworm);
  }


  return res_code;
}

/*Functions related explicitly to A-Star Algorithm*/

void reverseSearchMoveDeltas(struct worm* aworm, struct board* aboard, struct pos start, struct pos finish)
{
    enum Direction lastDir;
    while(start.x != finish.x && start.y != finish.y){
        lastDir = aboard->nodes[start.y][start.x].dir;
        start = addDirection(start, aboard->nodes[start.y][start.x].dir, true);
    }
    switch (lastDir){
        case UP:
            setWormHeading(aworm, WORM_UP);
            break;
        case DOWN:
            setWormHeading(aworm, WORM_DOWN);
            break;
        case LEFT:
            setWormHeading(aworm, WORM_LEFT);
            break;
        case RIGHT:
            setWormHeading(aworm, WORM_RIGHT);
            break;
        case START:
            showDialog("Dir = START", "");
            break;
    }
}

struct pos addDirectionXY(struct pos base_pos, int dx, int dy){
    base_pos.x += dx;
    base_pos.y += dy;
    return base_pos;
}

struct pos chooseNewPos(struct worm* aworm, struct board* aboard, struct pos cur_pos)
{
  /*
     Get node with lowest f. f = g + h.
     */
  struct f fCheapest = {1000, UP};
  struct pos temp;
  int i;
  for(i = 0; i < 4; i++){
    temp = addDirection(cur_pos, i, false);
    if(aboard->nodes[temp.y][temp.x].f < fCheapest.f
       && aboard->nodes[temp.y][temp.x].isActive) {
      fCheapest.f = aboard->nodes[temp.y][temp.x].f;
      fCheapest.direction = i;
    }
  }
  //setWormHeading(aworm, fCheapest.direction);
  return addDirection(cur_pos, fCheapest.direction, false);
}

bool getPossibleDirections(struct worm* aworm, struct board* aboard, struct pos cur_pos, struct pos dest_pos)
{
  bool hasDirection = false;
  struct pos up, down, left, right;
  up = addDirection(cur_pos, UP, false);
  down = addDirection(cur_pos, DOWN, false);
  left = addDirection(cur_pos, LEFT, false);
  right = addDirection(cur_pos, RIGHT, false);


  if(isGoodHeading(aboard, up)){
    fillNode(cur_pos, dest_pos, aboard, aboard->nodes[cur_pos.y][cur_pos.x].g, UP);
    hasDirection = true;
  }
  if(isGoodHeading(aboard, down)){
    fillNode(cur_pos, dest_pos, aboard, aboard->nodes[cur_pos.y][cur_pos.x].g, DOWN);
    hasDirection = true;
  }
  if(isGoodHeading(aboard, left)){
    fillNode(cur_pos, dest_pos, aboard, aboard->nodes[cur_pos.y][cur_pos.x].g, LEFT);
    hasDirection = true;
  }
  if(isGoodHeading(aboard, right)){
    fillNode(cur_pos, dest_pos, aboard, aboard->nodes[cur_pos.y][cur_pos.x].g, RIGHT);
    hasDirection = true;
  }
  return hasDirection;
}

void fillNode(struct pos current, struct pos food_pos, struct board* aboard, int prev_Cost, enum Direction dir)
{
  int heuristic = getHeuristic(current, food_pos);
  aboard->nodes[current.y][current.x].isActive = true;
  aboard->nodes[current.y][current.x].g = prev_Cost + 1; //Gesamte kosten berechnen
  aboard->nodes[current.y][current.x].h = heuristic;
  aboard->nodes[current.y][current.x].f = heuristic + prev_Cost +1;
  aboard->nodes[current.y][current.x].dir = dir;
}

int getHeuristic(struct pos pos_start, struct pos pos_finish)
{
  //Calculate Delta's
  struct pos result;
  result.x = pos_finish.x - pos_start.x;
  result.y = pos_finish.y - pos_start.y;
  return getDistance(result);
}

struct pos addDirection(struct pos base_pos, enum Direction dir, bool backwards){
  int back;
  if(backwards){
    back = -1;
  } else {
    back = 1;
  }
  struct pos result;
  switch (dir)  {
    case UP:
      result.x = base_pos.x;
      result.y = base_pos.y + (-1 * back);
      break;
    case DOWN:
      result.x = base_pos.x;
      result.y = base_pos.y + (1 * back);
      break;
    case LEFT:
      result.x = base_pos.x + (-1 * back);
      result.y = base_pos.y;
      break;
    case RIGHT:
      result.x = base_pos.x + (1 * back);
      result.x = base_pos.y;
      break;
  }
  return result;
};

