// A simple variant of the game Snake
//
// Used for teaching in classes
//
// Author:
// Franz Regensburger
// Ingolstadt University of Applied Sciences
// (C) 2011
//
// The board model
// *************************************************
// Placing and removing items from the game board
// Check boundaries of game board
// *************************************************

#include <curses.h>
#include <stdlib.h>
//Own Headers
#include "worm.h"
#include "board_model.h"
#include "messages.h"

// Place an item onto the curses display.
void placeItem(struct board* aboard, struct pos pos, enum BoardCodes board_code, 
              chtype symbol, enum ColorPairs color_pair) {

  //  Store item on the display (symbol code)
  move(pos.y, pos.x);                         // Move cursor to (y,x
  attron(COLOR_PAIR(color_pair));     // Start writing in selected color
  addch(symbol);                      // Store symbol on the virtual display
  attroff(COLOR_PAIR(color_pair));    // Stop writing in selected color
  aboard->cells[pos.y][pos.x] = board_code; //Set cells Array correct
}

enum ResCodes initializeLevel(struct board* aboard)
{
  struct pos position; 
  for (position.y = 0; position.y <= aboard->last_row; position.y++)
  {
    for (position.x = 0; position.x <= aboard->last_col; position.x++)
    {
      //Fill array with Free Cells
      placeItem(aboard, position, BC_FREE_CELL, SYMBOL_FREE_CELL, COLP_FREE_CELL);
    }
  }
  //Draw Line
  position.y = aboard->last_row + 1;
  for (position.x = 0; position.x <= aboard->last_col; position.x++)
  {
    move(position.y, position.x);
    attron(COLOR_PAIR(COLP_BARRIER));
    addch(SYMBOL_BARRIER);
    attroff(COLOR_PAIR(COLP_BARRIER));
  }
  position.x = aboard->last_col;
  //Draw line to signal the rightmost column of the board
  for(position.y = 0; position.y <= aboard->last_row; position.y++)
  {
    placeItem(aboard, position, BC_BARRIER, SYMBOL_BARRIER, COLP_BARRIER);
  }
  position.y = 0; 
  for(position.x = 0; position.x <= aboard->last_col; position.x++)
  {
    placeItem(aboard, position, BC_BARRIER, SYMBOL_BARRIER, COLP_BARRIER);
  }
  position.x = 0; 
  for(position.y = 0; position.y <= aboard->last_row; position.y++)
  {
    placeItem(aboard, position, BC_BARRIER, SYMBOL_BARRIER, COLP_BARRIER);
  }
  //Place Food
  int i; 
  position.y = 3;
  for (i = 1; i < 11; i++)
  {
    position.x = i; 
    placeItem(aboard, position, BC_FOOD_1, SYMBOL_FOOD_1, COLP_FOOD_1);
  }
  aboard->food_items = 10; 
  return RES_OK;
}

// Getters

// Get the last usable row on the display
int getLastRowOnBoard(struct board* aboard)
{
  return aboard->last_row;
}
// Get the last usable column on the display
int getLastColOnBoard(struct board* aboard) {
  return aboard->last_col;
}

int getNumberOfFoodItems(struct board* aboard)
{
  return aboard->food_items;
}

enum BoardCodes getContentAt(struct board* aboard, struct pos position)
{
  return aboard->cells[position.y][position.x];
}

void setNumberOfFoodItems(struct board* aboard, int n)
{
  aboard->food_items = n; 
}

void decrementNumberOfFoodItems(struct board* aboard)
{
  aboard->food_items--;
}

enum ResCodes initializeBoard(struct board* aboard)
{
  int y;
  //Maximal index of a row, reserve space for message area
  aboard->last_row = LINES - ROWS_RESERVED - 1;
  //Maximal index of a column
  aboard->last_col = COLS - 1;
  if( aboard->last_col < MIN_NUMBER_OF_COLS - 1 || 
      aboard->last_row < MIN_NUMBER_OF_ROWS - 1 )
  {
    char buf[100];
    sprintf(buf, "Das Fenster ist zu klein wir brauchen %dx%d",
    MIN_NUMBER_OF_COLS, MIN_NUMBER_OF_ROWS + ROWS_RESERVED);
    showDialog(buf, "Bitte eine Taste druecken");
    return RES_FAILED;
  }
  // Allocate memory for 2-dimensional array of cells
  // Alloc array of rows
  aboard->cells = malloc((aboard->last_row + 1) * sizeof(enum BoardCodes*));
  if(aboard->cells == NULL)
  {
    showDialog("Abbruch: Zu wenig Speicher", "Bitte eine Taste druecken");
    exit(RES_FAILED); //No memory --> WHERE TO STORE OUR DATA?-->Just get away!
  }
  for(y = 0; y <= aboard->last_col; y++)
  {
    //Allocate array of columns for each y
    aboard->cells[y] = malloc((aboard->last_col + 1) * sizeof(enum BoardCodes));
    if(aboard->cells[y] == NULL)
    {
      showDialog("Abbruch: Zu wenig Speicher", "Bitte eine Taste druecken");
      exit(RES_FAILED); //No memory --> WHERE TO STORE OUR DATA?-->Just get away!
    }

  }
  return RES_OK;
}

void cleanupBoard(struct board* aboard)
{
  int y = 0;
  for(y = 0; y < aboard->last_row; y++)
  {
    free(aboard->cells[y]);
  }
  free(aboard->cells);
}
