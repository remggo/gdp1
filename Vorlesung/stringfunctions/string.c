#include <stdlib.h>
#include <stdio.h>

size_t my_strlen(const char *s) {
  size_t counter =  0;
  while(s[counter] != '\0'){
    counter++; //funktioniert weil wenn s[counter] == \0 wurde schon 1 hinzugezählt: anfang bei 0 wieder ausgebügelt
  }
  return counter; 
}

char* strcpy(char* dest, const char* src){
  char* d = dest;
  size_t i;
  for(i = 0; src[i] != '\0'; i++){
    d[i] = src[i];
  }
  d[i] = '\0';
  return dest;
}

