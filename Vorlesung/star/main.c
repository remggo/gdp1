#include <stdio.h>
#define SPALTEN 15
//Globales Array

#define arrayAccess(ar, S, z, s) *((ar) + (z)*(S) + (s))
//Bei Makros --> IMMER GUT KLAMMERN!
void fuellen(char** einRahmen, int zeilen, int spalten)
{
  int i, j; 
  for(i = 0; i < zeilen; i++)
  {
    for(j = 0; j < spalten; j++)
    {
      *(einRahmen + i * spalten + j) = '*';
    }
  }
  
  for(i = 1; i < zeilen - 1; i++)
  {
    for(j = 1; j < spalten - 1; j++)
    {
      *(einRahmen + i * spalten + j) = ' ';
    }
  }
}

void drucken(char** einRahmen, int zeilen, int spalten)
{
  int i, j; 
  for( i = 0; i < zeilen; i++)
  { 
    for( j = 0; j < spalten; j++)
    {
      printf("%c ", *(einRahmen + i * spalten + j));
    }
    printf("\n");
  }
}

void machArm(char** einRahmen, int zeilen, int spalten, int coordX, int coordY, int dx, int dy)
{
  int neu_z = coordY;
  int neu_s = coordX;
  char code = 'A';
  while(*(einRahmen + neu_z * spalten + neu_s) != '*')
  {
   *(einRahmen +  neu_z * spalten + neu_s) = code;
    code++;
    neu_z += dy;
    neu_s += dx; 
  }
}

void stern(char** einRahmen, int zeilen, int spalten, int z0, int s0)
{
  //Arm nach Rechts
  machArm(einRahmen, zeilen, spalten, z0, s0, 0, 1);
  machArm(einRahmen, zeilen, spalten, z0, s0, 1, 1);
  machArm(einRahmen, zeilen, spalten, z0, s0, 1, 0);
  machArm(einRahmen, zeilen, spalten, z0, s0, 1,-1);
  machArm(einRahmen, zeilen, spalten, z0, s0, 0,-1);
  machArm(einRahmen, zeilen, spalten, z0, s0, -1, -1);
  machArm(einRahmen, zeilen, spalten, z0, s0, -1, 0);
  machArm(einRahmen, zeilen, spalten, z0, s0, -1, 1);
}

void usage(int argc, char** argv)
{
  //TODO:Belehrung des Nutzers
  printf("Trottl");
  return;
}

//hier ohne Fehlerbehandlung
char** allozieren(int zeilen, int spalten)
{
  char** ergebnis;  
  int i;

  ergebnis = malloc(zeilen * sizeof(char*));
  if(ergebnis != NULL)
  {
    for(i = 0; i < zeilen; i++)
    {
      ergebnis[i] = malloc(spalten * sizeof(char));
    }
  }  
  return ergebnis;
}

void freigeben(char** array, int zeilen)
{
  //Free ist es egal ob ein Nullpointer übergeben wird
  int i;
  for(i = 0; i < zeilen; i++)
  {
    free(array[i]);
  }
  free(array);
}

int main(int argc, char** argv)
{
  int z0, s0, zeilen, spalten; 

  //Aufruf bin/stern 30 20 7 12 (pgname zeilen spalten zentrumY zentrumX
  if(argc != 5)
  {
    usage(argc, argv);
    return 1; 
  }
  zeilen = atoi(argv[1]);
  spalten= atoi(argv[2]);
  z0 = atoi(argv[3]);
  s0 = atoi(argv[4]); 

  if( zeilen < 3    || 
      spalten < 3   || 
      z0 >= zeilen  || 
      z0 <= 0       || 
      s0 >= spalten ||
      s0 <= 0)
  {
    usage(argc, argv);
    return 1; 
  }

  char** Origrahmen;
  Origrahmen = allozieren(zeilen, spalten);
  if(Origrahmen == NULL)
  {
    printf("Kauf dir Speicher!");
    return 1;
  }

  // char *pRahmen[]; 
  rahmen = Origrahmen;

  fuellen(rahmen, zeilen, spalten);
  stern(rahmen, zeilen, spalten, z0, s0);
  drucken(rahmen, zeilen, spalten); 

  freigeben();
  return 0;
}
