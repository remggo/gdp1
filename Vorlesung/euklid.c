// Verbesserung der Version euklid-V4.1
// ggT erlaubt nun auch negative Zahlen als Argumente

#include <stdio.h>
#include <stdbool.h>
#include <curses.h>

#define OK 0
#define ERROR 1

int ggT(int a, int b) {
    if (a < 0)
    {
      a = -a;
    }
    if (b < 0)
    {
      b = -b;
    }
    while (a != b) 
    {
        if (a < b)
            b = b - a;
        else
            a = a - b;
    }
    return a;
}

int readInteger(char prompt[], int *addr)
{
  printf("%s", prompt);
  fflush(stdout);
  
  int zaehler = 3; 
  bool erfolg = false; 

  while(!erfolg && zaehler != 0)
  {
    if(scanf("%d", addr) != 1) 
    {
      printf("Fehler bei der Eingabe der Zahl\n");
      zaehler--;
    }
    while(getch() != '\n');
    erfolg = true;
  }
  
  if(erfolg)
    return OK;
  else
    return ERROR; 
}


int main (void) {
    int i1;
    int i2;

    /* Lies zwei ganze Zahlen in i1 und i2 ein */
    if( readInteger("i1=", &i1) != OK)
    {
      printf("Fehler bei Eingabe i1");
      return ERROR; 
    }
    
    if( readInteger("i2=", &i2) != OK)
    {
      printf("Fehler bei Eingabe i2");
      return ERROR; 
    }
    printf ("Der groesste gemeinsame Teiler von %d und %d ist: %d\n", i1,i2,ggT(i1,i2));
    return OK;
}
