#include<stdio.h>
#include<stdlib.h>

int main(int argc, char *argv[])
{
  int i; 
  int ergebnis = 0; 
  printf("Das Ergebnis von ");
  for(i = 1; i < argc; i++)
  {
    if(i < argc - 1)
    {
      printf("%d + ", atoi(argv[i])); 
    }
    else
    {
      printf("%d ", atoi(argv[i]));
    }
    ergebnis += atoi(argv[i]); 
  }
  printf("= %d\n", ergebnis); 
  return 0; 
}
