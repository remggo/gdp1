bool list_contains_iter(node_t *node, int data) {
  bool result = false;
  while(node != NULL){
    if(node->data == data){
      result =  true; 
      break;
    }
    node = node->next;
  }
  return result;
}
